﻿using RabbitMQ.Client;
using System;
using System.Text;

class Program
{
    static void Main()
    {
        var factory = new ConnectionFactory()
        {
            HostName = "localhost",
            Port = 5672,
            UserName = "guest",
            Password = "guest",
        };

        using (var connection = factory.CreateConnection())
        using (var channel = connection.CreateModel())
        {
            channel.QueueDeclare("router_queue", durable: false, exclusive: false, autoDelete: false, arguments: null);

            while (true)
            {
                Console.WriteLine("Select message type:");
                Console.WriteLine("1. Widget");
                Console.WriteLine("2. Gadget");
                Console.Write("Enter your choice: ");
                string messageTypeChoice = Console.ReadLine();

                string messageType;
                if (messageTypeChoice == "1")
                {
                    messageType = "widget";
                }
                else if (messageTypeChoice == "2")
                {
                    messageType = "gadget";
                }
                else
                {
                    Console.WriteLine("Invalid choice. Please select 1 for Widget or 2 for Gadget.");
                    continue;
                }

                Console.Write("Enter message content: ");
                string messageContent = Console.ReadLine();


                var properties = channel.CreateBasicProperties();
                properties.Headers = new System.Collections.Generic.Dictionary<string, object>
                {
                    { "type", Encoding.UTF8.GetBytes(messageType) }
                };

                channel.BasicPublish(
                    exchange: "",
                    routingKey: "router_queue",
                    basicProperties: properties,
                    body: Encoding.UTF8.GetBytes(messageContent)
                );

                Console.WriteLine($"Sent: Type={messageType}, Content={messageContent}");
            }
        }
    }
}
