﻿using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System;
using System.Text;

var factory = new ConnectionFactory()
{
    HostName = "localhost",
    Port = 5672,
    UserName = "guest",
    Password = "guest",
};

using (var connection = factory.CreateConnection())
using (var channel = connection.CreateModel())
{
    channel.QueueDeclare("router_queue", durable: false, exclusive: false, autoDelete: false, arguments: null);

    var consumer = new EventingBasicConsumer(channel);

    consumer.Received += (model, ea) =>
    {
        var body = ea.Body.ToArray();
        var message = Encoding.UTF8.GetString(body);
        var type = ea.BasicProperties.Headers.ContainsKey("type")
            ? Encoding.UTF8.GetString(ea.BasicProperties.Headers["type"] as byte[])
            : null;

        Console.WriteLine($"Received: {message}, Type: {type}");

        if (type == "widget")
        {
            channel.BasicPublish(exchange: "", routingKey: "widget_queue", basicProperties: null, body: body);
        }
        else if (type == "gadget")
        {
            channel.BasicPublish(exchange: "", routingKey: "gadget_queue", basicProperties: null, body: body);
        }
        else
        {
            // Handle other routing.
        }
    };

    channel.BasicConsume("router_queue", autoAck: true, consumer);
    
    Console.WriteLine("Press [Enter] to exit.");
    Console.ReadLine();
}
